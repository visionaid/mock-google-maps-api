import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Logger,
  Post,
} from '@nestjs/common';
import { PlacesService } from './places.service';
import { GetNearbySearchDTO } from './dtos/getNearbySearch.dto';

@Controller('places')
export class PlacesController {
  private readonly logger = new Logger(PlacesController.name);

  constructor(private readonly placesService: PlacesService) {}

  @Post('stores')
  async getNearby(@Body() nearbySearch: GetNearbySearchDTO) {
    try {
      this.logger.log(
        `Getting nearby stores for coordinates: ${nearbySearch.userCoordinates.lat}, ${nearbySearch.userCoordinates.lon}`,
      );

      return this.placesService.findNearbyStores(
        nearbySearch.userCoordinates,
        nearbySearch.radius,
      );
    } catch (error) {
      this.logger.error('Failed to retrieve nearby stores', error);
      throw new HttpException(
        'Failed to retrieve nearby stores',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
