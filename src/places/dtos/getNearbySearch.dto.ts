import { Type } from 'class-transformer';
import {
  IsInt,
  IsNumber,
  IsObject,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';

class CoordinatesDTO {
  @IsNumber()
  lat: number;

  @IsNumber()
  lon: number;
}

export class GetNearbySearchDTO {
  @IsObject()
  @ValidateNested()
  @Type(() => CoordinatesDTO)
  userCoordinates: CoordinatesDTO;

  @IsInt()
  @Min(1)
  @Max(2000)
  radius: number;
}
