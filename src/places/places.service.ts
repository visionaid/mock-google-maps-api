/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from '@nestjs/common';
import * as findNearbyStores from './mocked-responses/findNearbyStores.json';

@Injectable()
export class PlacesService {
  async findNearbyStores(
    userCoordinates: { lat: number; lon: number },
    radius: number,
  ): Promise<any> {
    return findNearbyStores;
  }
}
