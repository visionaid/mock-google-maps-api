import { Module } from '@nestjs/common';
import { PedestrianRouteController } from './pedestrian-route.controller';
import { PedestrianRouteService } from './pedestrian-route.service';

@Module({
  controllers: [PedestrianRouteController],
  providers: [PedestrianRouteService],
})
export class PedestrianRouteModule {}
