import { Injectable } from '@nestjs/common';
import * as getRoute from './mocked-responses/getRoute.json';

@Injectable()
export class PedestrianRouteService {
  async getRouteFromOpenRouteService(): Promise<any> {
    return getRoute;
  }

  async getRouteSimpleVersion(): Promise<any> {
    return {
      route:
        'Kieruj się w kierunku północnym przez 100 metrów, a następnie skręć w lewo.',
    };
  }
}
