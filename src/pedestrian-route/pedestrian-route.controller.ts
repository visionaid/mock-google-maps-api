import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Logger,
  Query,
} from '@nestjs/common';
import { PedestrianRouteService } from './pedestrian-route.service';
import { IsString } from 'class-validator';

@Controller('pedestrian-route')
export class PedestrianRouteController {
  private readonly logger = new Logger(PedestrianRouteController.name);

  constructor(
    private readonly pedestrianRouteService: PedestrianRouteService,
  ) {}

  @Get()
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async getRoute(@Query('start') start: string, @Query('end') end: string) {
    try {
      this.logger.log(`Getting route from ${start} to ${end}`);

      return await this.pedestrianRouteService.getRouteFromOpenRouteService();
    } catch (e) {
      this.logger.error('Failed to retrieve pedestrian route', e.stack);

      throw new HttpException(
        'Failed to retrive pedestrian route',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}

export class GetRouteDto {
  @IsString()
  start: string;

  @IsString()
  end: string;
}
