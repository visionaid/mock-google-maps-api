import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GeocodingModule } from './geocoding/geocoding.module';
import { PedestrianRouteModule } from './pedestrian-route/pedestrian-route.module';
import { PlacesModule } from './places/places.module';

@Module({
  imports: [GeocodingModule, PedestrianRouteModule, PlacesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
