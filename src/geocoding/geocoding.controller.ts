import { Controller, Get, Logger, Query } from '@nestjs/common';
import { GeocodingService } from './geocoding.service';
import { GetLocationInfoQueryDto } from './dtos/get-location-info.dto';

@Controller('geocoding')
export class GeocodingController {
  private readonly logger = new Logger(GeocodingController.name);

  constructor(private readonly geocodingService: GeocodingService) {}

  // convert coordinates to location info
  @Get('location-info')
  async getLocationInfo(
    @Query() { latitude, longitude }: GetLocationInfoQueryDto,
  ) {
    this.logger.log(
      `Getting location info for coordinates: ${latitude}, ${longitude}`,
    );

    return this.geocodingService.getLocationInfoSimple(latitude, longitude);
  }

  // convert locality to coordinates
  @Get('coordinates')
  async getCoordinates(@Query('locality') locality: string) {
    return this.geocodingService.getCoordinates(locality);
  }
}
