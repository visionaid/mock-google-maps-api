import { Test, TestingModule } from '@nestjs/testing';
import { GeocodingController } from './geocoding.controller';
import { GeocodingService } from './geocoding.service';
import { HttpModule } from '@nestjs/axios';
import { GetLocationInfoResponseDto } from './dtos/get-location-info.dto';
import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';

describe('GeocodingController', () => {
  let app: INestApplication;
  let geocodingService: GeocodingService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [GeocodingController],
      providers: [GeocodingService],
    })
      .overrideProvider(GeocodingService)
      .useValue({
        getLocationInfo: jest.fn().mockImplementation((lat, lon) =>
          Promise.resolve({
            place_id: 123,
            licence: 'Sample License',
            osm_type: 'way',
            osm_id: 456,
            lat: String(lat),
            lon: String(lon),
            display_name: 'Sample Place',
            // include other properties as per your GetLocationInfoResponseDto
          } as GetLocationInfoResponseDto),
        ),
      })
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    geocodingService = moduleFixture.get<GeocodingService>(GeocodingService);
  });

  it('/GET geocoding/location-info', () => {
    return request(app.getHttpServer())
      .get('/geocoding/location-info?latitude=40.714224&longitude=-73.961452')
      .expect(200)
      .expect({
        place_id: 123,
        licence: 'Sample License',
        osm_type: 'way',
        osm_id: 456,
        lat: '40.714224',
        lon: '-73.961452',
        display_name: 'Sample Place',
        // match the expected structure of your GetLocationInfoResponseDto
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
