import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map, catchError, lastValueFrom } from 'rxjs';
import { GetLocationInfoResponseDto } from './dtos/get-location-info.dto';

@Injectable()
export class GeocodingService {
  constructor(private http: HttpService) {}

  async getLocationInfo(
    latitude: number,
    longitude: number,
  ): Promise<GetLocationInfoResponseDto> {
    const observable = this.http
      .get(
        `https://nominatim.openstreetmap.org/reverse?lat=${latitude}&lon=${longitude}&format=json`,
      )
      .pipe(
        map((response) => response.data),

        catchError((error) => error),
      );

    return await lastValueFrom(observable);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async getLocationInfoSimple(latitude: number, longitude: number) {
    return {
      display_name:
        '15-17d, Plac Józefa Piłsudskiego, Nadodrze, Opole, województwo opolskie, 45-706, Polska',
      address: {
        house_number: '15-17d',
        road: 'Plac Józefa Piłsudskiego',
        suburb: 'Nadodrze',
        city: 'Opole',
        state: 'województwo opolskie',
        'ISO3166-2-lvl4': 'PL-16',
        postcode: '45-706',
        country: 'Polska',
        country_code: 'pl',
      },
    };
  }

  async getCoordinates(locality: string) {
    const encodedLocality = encodeURIComponent(locality);

    return this.http
      .get(
        `https://nominatim.openstreetmap.org/search?q=${encodedLocality}&q=${locality}&format=json`,
      )
      .pipe(
        map((response) => response.data),
        catchError((error) => error),
      );
  }
}
