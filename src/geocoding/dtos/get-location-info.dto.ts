import { IsLatitude, IsLongitude } from 'class-validator';

export class GetLocationInfoQueryDto {
  @IsLatitude()
  latitude: number;

  @IsLongitude()
  longitude: number;
}

export class GetLocationInfoResponseDto {
  place_id: number;
  licence: string;
  osm_type: string;
  osm_id: number;
  lat: string;
  lon: string;
  class: string;
  type: string;
  place_rank: number;
  importance: number;
  addresstype: string;
  name: string;
  display_name: string;
  address: {
    house_number?: string;
    road: string;
    suburb: string;
    city: string;
    state: string;
    'ISO3166-2-lvl4'?: string;
    postcode: string;
    country: string;
    country_code: string;
  };
  boundingbox: string[];
}
